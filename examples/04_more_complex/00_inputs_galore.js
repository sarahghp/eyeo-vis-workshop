// resets & setup — evaluate once

resetBuffer = (buffer) => solid().out(buffer);

resetAll = () => {
  [o0, o1, o2, o3].forEach(
    (buffer) => {
      solid().out(buffer)
    }
  );
  [s0, s1, s2, s3].forEach(
    (source) => {
      source.clear()
    }
  );
  a.hide();
};
 
resetAll() 

// in our last example, we looked at modulation based on other output buffers
// one of my favorite sources to use is the external screen

s0.initScreen(2);

src(s0).out(o0)

src(s0)
  .modulateKaleid(osc(11,0.5,0),2)
.out()

src(s0)
  .modulateRotate(osc(11,0.5,0),2)
  // .modulateKaleid(osc(11,0.5,0),2)
.out()

render(o0)
resetAll() 

// we can also use an image (and video)

myImg = document.createElement('img'); // <img />

myImg.src = '/Users/boopers/codex/hydra-examples/eyeo-vis-workshop/examples/__img/rotate-away-5.gif';    // <img src=" " />

myVid = document.createElement('video');

myVid.src = '/Users/boopers/codex/hydra-examples/eyeo-vis-workshop/examples/__img/rotate-away-5.mp4';

s1.init(
  {
    src: myImg, // variable that holds element
    dynamic: false   // set true for video
  }
);

src(s1)
.scrollX(0, 0.01)
.kaleid(4)
.out();

s1.clear()


// audio — a is the inbuilt audio object
a.show();
a.setBins(4);

shape( 3 )
.rotate(
  () => Math.PI * mouse.x / 180
)
.repeatX( 3 )
.repeatY( 3 )
.scale(
  () => a.fft[0]*4
)
// .rotate(
//   () => a.fft[1]*10
// )
.out( o0 );

render(o0);
resetAll();

a.hide();

// webcam

s0.initCam();

src(s0)
  .thresh(.2)
  .out(o0);

render(o0);

s0.clear();