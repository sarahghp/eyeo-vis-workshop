# Livecoding Visuals with Hydra

These are the example files for the Eyeo Livecoding Workshop. Held Monday, June 3, in Minneapolis.

## Hydra on the Web

[Go to here 👉](https://hydra-editor.glitch.me/)

## Installing the Atom Plugin

**Using Atom:**
* Download and install [Atom](https://atom.io), an open source text editor built by GitHub.
* Install inside Atom:
  * Open _Preferences_
  * Click _Install_ on left sidebar
  * Search for "atom-hydra" using search bar and click install
  * Restart atom editor
* Install using APM (Atom Package Manager):
  * The Atom Package Manager is a Command Line tool that you can use to install atom plugins
  * Open terminal
  * Run `apm install atom-hydra`

To start Hydra inside Atom, click _Packages_ in top menu bar. Under "atom-hydra" select _Toggle_. Press _Control_ + _Option_ + _Enter_ to evaluate a block of code.

([instructions from Zach's Hydra Worksop](https://github.com/zachkrall/hydra-workshop))
